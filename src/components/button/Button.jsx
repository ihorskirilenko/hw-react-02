import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Button.scss';


class Button extends Component {

    render() {

        return(
            <button
                type='button'
                onClick={this.props.onClick}
            >{this.props.text}</button>
        )
    }
}

export default Button;

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}