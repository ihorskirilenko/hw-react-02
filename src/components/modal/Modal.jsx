import { Component } from 'react';
import PropTypes from 'prop-types';
import { faSquareXmark } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import './Modal.scss';


class Modal extends Component {

    render() {
        return (
            <div className="modalBackdrop" onClick={this.props.onModalClose}>
                <div className="modalWrapper" onClick={(ev) => ev.stopPropagation()}>
                    <div className="modalHeader">
                        <h4>{this.props.text}</h4>
                        <FontAwesomeIcon
                            icon={ faSquareXmark }
                            onClick={this.props.onModalClose}
                        />
                    </div>
                    <span className="modalText">{this.props.header}</span>
                    <div className="modalButtons">
                        {this.props.actions}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onModalClose: PropTypes.func.isRequired,
    actions: PropTypes.element.isRequired
}
