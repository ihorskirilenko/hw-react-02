import './ProductCard.scss';
import React, { Component } from "react";
import Button from "../button/Button";
import { faHeart, faStar } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import Modal from "../modal/Modal";

class ProductCard extends Component {

    state = {
        isProductLiked: this.props.liked || false
    }

    render() {

        const { id, name, price, imageUrl, color } = this.props.product
        const { activateModal, toggleLiked } = this.props

        return (

            <div className="card">

                <div className="cardImage"
                     style={{
                         backgroundImage: `url(${imageUrl})`
                     }}>
                    <FontAwesomeIcon
                        icon={ faStar }
                        onClick={() => {
                            toggleLiked(this.props.product, this.state.isProductLiked);
                            this.setState((state) => { return {isProductLiked: !state.isProductLiked }})
                        }}
                        style={{
                            color: `${this.state.isProductLiked ? "red" : "#1165B2"}`
                        }}

                    />
                </div>

                <h3 key={name}>{name}</h3>

                <div className="cardColor">
                    <p>Available color: </p>
                    <div className="cardColor" style={{ backgroundColor: `${color}` }}/>
                </div>

                <p className="cardPrice" key={id}>$ {price}</p>

                <Button
                    text="Add to cart"
                    onClick={() => {
                        activateModal(this.props.product)
                    }}
                />

            </div>
        )
    }
}


export default ProductCard

ProductCard.propTypes = {
    product: PropTypes.shape({
        code: PropTypes.number,
        color: PropTypes.string,
        id: PropTypes.number,
        imageUrl: PropTypes.string,
        price: PropTypes.number
    }).isRequired,
    activateModal: PropTypes.func.isRequired,
    toggleLiked: PropTypes.func.isRequired

}
