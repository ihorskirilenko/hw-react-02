import './Products.scss';
import ProductCard from "../productCard/ProductCard";
import React, { Component } from "react";
import PropTypes from "prop-types";

class Products extends Component {

    constructor(props) {
        super(props);
        this.state = { products: [] }
    }

    componentDidMount() {

        fetch('data.json')
            .then(response => response.json())
            .then(data => this.setState({ products: data }))

    }


    render() {

        const products = this.state.products

        return (
            <div className='productsList'>
                {products.map(el => <ProductCard
                    product={el}
                    key={el.id}
                    activateModal={this.props.activateModal}
                    toggleLiked={this.props.toggleLiked}
                    liked={this.props.liked.map(liked => liked.id).includes(el.id)}
                />)}
            </div>
        );
    }
}

export default Products;

Products.propTypes = {
    activateModal: PropTypes.func.isRequired,
    toggleLiked: PropTypes.func.isRequired,
    liked: PropTypes.array
}

Products.defaultProps = {
    liked: []
}
