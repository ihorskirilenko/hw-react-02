import React, { Component } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faCartShopping, faStar } from "@fortawesome/free-solid-svg-icons"
import './Header.scss';

class Header extends Component {


    render() {

        return(
            <>
                <div className="header">
                    <div className="headerTitle">
                        <h1>Kosher</h1>
                        <h2>motorcycles</h2>
                    </div>
                    <div style={{
                        flexGrow: 1
                    }}/>
                    <div className="headerCart">
                        <FontAwesomeIcon icon={ faCartShopping } />
                        {this.props.cart.length > 0 && <div className="headerQuantity">{this.props.cart.length}</div>}
                    </div>
                    <div className="headerWishList">
                        <FontAwesomeIcon icon={ faStar } />
                        {this.props.liked.length > 0 && <div className="headerQuantity">{this.props.liked.length}</div>}
                    </div>
                </div>
                <div className="headerLine"></div>
            </>
        )
    }
}

export default Header;

Header.propTypes = {
    cart: PropTypes.array,
    liked: PropTypes.array,
}

Header.defaultProps = {
    cart: [],
    liked: []
}
