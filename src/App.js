import React, { Component } from "react";
import Products from "./components/product/Products";
import Header from "./components/header/Header";
import Modal from "./components/modal/Modal";
import Button from "./components/button/Button";
import './App.scss';


class App extends Component {

    state = {
        isModalActive: false,
        productModal: {},
        cart: JSON.parse(localStorage.getItem('cart')) || [],
        liked: JSON.parse(localStorage.getItem('liked')) || []
    }


    activateModal = (product) => {
        this.setState({ isModalActive: true })
        this.setState({ productModal: product })
    }


    deActivateModal = () => { this.setState({ isModalActive: false }) }


    addProductToCart = (product) => {

        const prevCartStore = JSON.parse(localStorage.getItem('cart'))

        prevCartStore ? localStorage.setItem('cart', JSON.stringify([...prevCartStore, product])) : localStorage.setItem('cart', JSON.stringify([product]))
        this.setState({ cart: JSON.parse(localStorage.getItem('cart')) })

        this.deActivateModal()

    }


    toggleLiked = (product, isProductLiked) => {

        let liked = []

        if (isProductLiked) {
            liked = Array.from(new Set([...this.state.liked])).filter(el => el.id !== product.id)
        } else {
            liked = Array.from(new Set([...this.state.liked, product]))
        }

        localStorage.setItem('liked', JSON.stringify(liked))
        this.setState({ liked: liked })

    }

    render() {

      return (
          <>

              <Header cart={this.state.cart} liked={this.state.liked}/>

              <Products
                  activateModal={this.activateModal}
                  toggleLiked={this.toggleLiked}
                  liked={this.state.liked}
              />

              { this.state.isModalActive && <Modal
                  header={`Buy product ${this.state.productModal.code}?`}
                  text={`Do you really want to purchase ${this.state.productModal.name}?`}
                  onModalClose={this.deActivateModal}
                  actions={
                      <div className="modalButtons">
                          <Button
                              text="Add"
                              onClick={() => this.addProductToCart(this.state.productModal)}/>
                          <Button
                              text="Cancel"
                              onClick={this.deActivateModal}/>
                      </div>
                  }

              />}
          </>
     );
  }
}

export default App;
